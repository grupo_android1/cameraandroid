package com.meza.usocamaraandroid;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    // la declaración de objetos sea a nivel de clase
    ImageView ivphoto;
    Button btcapturar;
    ActivityResultLauncher<Intent> capturarFoto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ivphoto = findViewById(R.id.ivPhoto);
        btcapturar = findViewById(R.id.bcapture);

        String[] permisos = new String[]{

                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,

        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permisos, 1);
        }
       btcapturar.setOnClickListener(this);

        capturarFoto = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        //Aqui va lo que sucede cuando se obtuvo el resultado de la ejecución
                        //de la cámara

                        if (result.getResultCode() == RESULT_OK) {

                            Bundle extras = result.getData().getExtras();
                            //DATA CONTIENE LA IMAGEN
                            Bitmap unaFoto = (Bitmap) extras.get("data");
                            ivphoto.setImageBitmap(unaFoto);
                        }
                    }
                });
    }

        private void tomarFoto () {

            capturarFoto.launch(new Intent(MediaStore.ACTION_IMAGE_CAPTURE));
        }


        @Override
        public void onClick (View view){
            //Realizar las acciones solo si se asignaron los permisos

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                //Vamos a utilizar la cámara
                tomarFoto();
            } else {

                Toast.makeText(this, "No se puede usar la cámara", Toast.LENGTH_SHORT).show();
            }
        }

}
